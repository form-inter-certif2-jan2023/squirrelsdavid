---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Check primary color is ok

Cette fonction va permettre de vérifier si les couleurs de fourrure (primary_fur_color) présentes dans le dataset sont valides.

Elle reçoit en entrée un vecteur de couleurs et renvoie un booleen indiquant si l'ensemble des couleurs sont valides ou non.

```{r function-check_primary_color_is_ok}
#' Verification des couleurs de fourrure
#'
#' @param string Un vecteur contenant les couleurs de fourrure a verifier.
#' @return Un boolean. L'ensemble des couleurs sont elles correctes ou non.
#' 
#' @export
#'
#' @examples
check_primary_color_is_ok <- function(string) {
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
  if (isFALSE(all_colors_OK)) {
    stop("Certaines couleurs ne sont pas conformes.")
  }
  
  return(all_colors_OK)
}
```

```{r example-check_primary_color_is_ok, eval=FALSE}
check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
#' \dontrun{
check_primary_color_is_ok(string = c("Blue", "Cinnamon", "Black", NA))
#' }
```

```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
  )
  expect_error(
    object = check_primary_color_is_ok(string = c("Blue", "Cinnamon", "Black", NA)),
    regexp = "Certaines couleurs ne sont pas conformes."
  )
})
```

# Check squirrel data integrity

* La fonction vérifie que les données contiennent une colonne primary_fur_color, 
et cette colonne ne doit contenir que des couleurs autorisées (voir votre fonction check_primary_color_is_ok())
* Arrêter la fonction si ce n’est pas le cas
* Retourner un message si tout est correct

```{r function-check_squirrel_data_integrity}
#' Check squirrel data integrity
#' 
#' La fonction vérifie que les données contiennent une colonne primary_fur_color, 
#' et cette colonne ne doit contenir que des couleurs autorisées
#' 
#' @param squirrels_df Un data.frame qui contient au moins une colonne primary_fur_color.
#' 
#' @return Rien. Renvoie une erreur si les données ne sont pas conformes et un message
#' si elles le sont.
#' 
#' @export
check_squirrel_data_integrity <- function(squirrels_df){
  primary_fur_color_present <- "primary_fur_color" %in% names(squirrels_df)
  
  if (isFALSE(primary_fur_color_present)) {
    stop("Attention, `squirrels_df` doit contenir la colonne `primary_fur_color`")
  }
  
  check_primary_color_is_ok(string = squirrels_df[["primary_fur_color"]])
  
  message("Les donn\u00e9es sont conformes.")
  
}
```

```{r example-check_squirrel_data_integrity}
squirrels_df <- readr::read_csv(
  # Le chemin vers mes donnés d'exemple
  system.file("nyc_squirrels_sample.csv", package = "squirrelsdavid")
)
check_squirrel_data_integrity(
  squirrels_df = squirrels_df
)
```

```{r tests-check_squirrel_data_integrity}
## Exemples qui fonctionnent pas
test_that("check_squirrel_data_integrity() les erreurs sont d\u00e9tect\u00e9es", {
  # Colonne primary_fur_color manquante
  expect_error(
    object = check_squirrel_data_integrity(squirrels_df = data.frame(a = 1)), 
    regexp = "Attention, `squirrels_df` doit contenir la colonne `primary_fur_color`"
  )
  # Couleurs non conformes
  expect_error(
    object = check_squirrel_data_integrity(data.frame(primary_fur_color = c("Rose"))),
    regexp = "Certaines couleurs ne sont pas conformes."
  )
})

## Exemples qui fonctionnent
test_that("check_squirrel_data_integrity() fonctionne comme attendu", {
  squirrels_df <- readr::read_csv(
    # Le chemin vers mes donnés d'exemple
    system.file("nyc_squirrels_sample.csv", package = "squirrelsdavid")
  )
  # Si la colonne primary_fur_color est présente il n'y a pas d'erreur
  expect_error(
    object = check_squirrel_data_integrity(squirrels_df = squirrels_df),
    regexp = NA
  )
  # Si les couleurs sont conformes on a bien le bon message
    expect_message(
    object = check_squirrel_data_integrity(squirrels_df = squirrels_df),
    regexp = "Les donn\u00e9es sont conformes."
  )
})

```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_check_data.Rmd", 
  vignette_name = "Check Data"
)
```


# WARNING - Generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand

test_that("study_activity works", {
  
  output <- study_activity(
    df_squirrels_act = data_act_squirrels, 
    col_primary_fur_color = "Gray"
  )
  
  expect_s3_class(
    output[["table"]],
    "data.frame"
  )
  expect_s3_class(
    output[["graph"]],
    c("gg", "ggplot")
  )
  
  # expect_equal(
  #   head(output[["table"]]),
  #   structure(
  #     list(
  #       age = c("Juvenile", "Juvenile", "Juvenile", "Adult"),
  #       primary_fur_color = c("Gray", "Gray", "Gray", "Gray"),
  #       activity = c("climbing", "running", "chasing", "chasing"),
  #       counts = c(65, 64, 30, 195)
  #     ),
  #     row.names = c(NA,-4L),
  #     class = c("tbl_df", "tbl", "data.frame")
  #   )
  # )
})

test_that("Les gardes-fous des study_activity() marchent", {
  
  expect_error(
    object = study_activity(
      df_squirrels_act = 1,
      col_primary_fur_color = "Gray"
    ),
    regexp = "`df_squirrels_act` doit \u00eatre un data.frame"
  )
  expect_error(
    object = study_activity(
      df_squirrels_act = data_act_squirrels,
      col_primary_fur_color = 1 
    ),
    regexp = "`col_primary_fur_color` doit \u00eatre un character"
  )
})


<!-- README.md is generated from README.Rmd. Please edit that file -->

![Le nom de l’image](./man/figures/squirrel.jpg)

# squirrelsdavid

<!-- badges: start -->
<!-- badges: end -->

Le but du package est d’affiché un message concernant la couleur des
écureuils de Central Park à New York.

## Installation

You can install the development version of squirrelsdavid like so:

``` r
# install.packages("squirrelsdavid")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsdavid)
## basic example code
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

``` r
get_message_fur_color("Black")
#> Interessons nous aux ecureuils de couleur Black
```

``` r
check_primary_color_is_ok("Black")
#> [1] TRUE
```
